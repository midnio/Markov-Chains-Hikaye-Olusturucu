# -*- coding=utf-8 -*-
#!/usr/bin/env python

try:
    import json
    import os
except: exit()
def loadd(filen):
    if not os.path.exists(filen):
        with open(filen,'w') as f:
            json.dump({},f)
    with open(filen, 'r') as f:
        dicti = json.load(f)
    return dicti
def learn(dicti,inp):
    toks = inp.split()
    for n in range(0,len(toks)-1):
        cw = toks[n]
        nw = toks[n+1]
        if cw not in dicti:
            dicti[cw] = { nw:1 }
        else:
            allnw = dicti[cw]
            if nw not in allnw:
                dicti[cw][nw] = 1
            else:
                dicti[cw][nw] = dicti[cw][nw] + 1
    return dicti
def upd(fname,dicti):
    with open(fname, 'w') as f:
        json.dump(dicti,f)
def main():
    dicti = loadd('dict.json')
    while 1:
        i = str(input("> "))
        if i == 'quittoris':
            break
        elif i == '':
            continue
        dicti = learn(dicti, i)
        upd('dict.json',dicti)
if __name__ == "__main__":
    main()
    input()