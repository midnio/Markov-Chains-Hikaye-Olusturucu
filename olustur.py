try:
    import os
    import sys
    import json
    import random
except: exit()
def ra():
    length = 50
    filen = 'dict.json'
    na = len(sys.argv)-1
    if na >= 1:
        length = int(sys.argv[1])
    if na >= 2:
        filen = sys.argv[2]
    return length, filen
def loadd(filen):
    if not os.path.exists(filen):
        quit("Unknown file.")
    with open(filen, 'r') as f:
        dicti = json.load(f)
    return dicti
def pickr(dicti):
    rn = random.randint(0, len(dicti)-1)
    nw = list(dicti.keys())[rn]
    return nw
def getnw(lw, dicti):
    if lw not in dicti:
        nw = pickr(dicti)
        return nw
    else:
        cand = dicti[lw]
        candn = []
        for word in cand:
            freq = cand[word]
            for n in range(0, freq):
                candn.append(word)
        rnd = random.randint(0, len(candn)-1)
        return candn[rnd]
def main():
    length, filen = ra()
    dicti = loadd(filen)
    lastw = "~~~~~~~~~~~"
    result = ''
    for i in range(0, length):
        nw = getnw(lastw, dicti)
        result = result + ' ' + nw
        lastw = nw
    print(result)
if __name__ == "__main__":
    main()